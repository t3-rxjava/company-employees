package com.virtusa.training.TrainingRest.model;

import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employee {


    private int id;
    private String name;

    public Employee(){    }
    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
