package com.virtusa.training.TrainingRest.controller;

import com.virtusa.training.TrainingRest.model.Employee;
import com.virtusa.training.TrainingRest.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public Iterable<Employee> list(){
        return employeeService.listAllEmployees();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> get(@PathVariable Integer id){
        try{
            Employee e = employeeService.getEmployee(id);
            return new ResponseEntity<>(e, HttpStatus.OK);
        } catch (NoSuchElementException e){
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employees")
    public void add(@RequestBody Employee employee){
        employeeService.saveEmployee(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<?> update(@RequestBody Employee employee, @PathVariable Integer id) {
        try {
            Employee existEmployee = employeeService.getEmployee(id);
            employee.setId(id);
            employeeService.saveEmployee(employee);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/employees/{id}")
    public void delete(@PathVariable Integer id) {

        employeeService.deleteEmployee(id);
    }


}