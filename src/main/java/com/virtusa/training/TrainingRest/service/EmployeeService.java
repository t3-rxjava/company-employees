package com.virtusa.training.TrainingRest.service;

import com.virtusa.training.TrainingRest.model.Employee;
import com.virtusa.training.TrainingRest.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Iterable<Employee> listAllEmployees(){

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return Arrays.asList(new Employee(101, "Raj"),
                new Employee(102, "Dev"),
                new Employee(150, "Sam"));
    }

    public void saveEmployee(Employee employee){
        employeeRepository.save(employee);
    }

    public Employee getEmployee(Integer id){
        return employeeRepository.findById(id).get();
    }

    public void deleteEmployee(Integer id){
        employeeRepository.deleteById(id);
    }
}