package com.virtusa.training.TrainingRest.repository;

import com.virtusa.training.TrainingRest.model.Employee;
import org.springframework.data.repository.CrudRepository;


public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
